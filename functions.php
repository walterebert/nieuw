<?php
/**
 * Theme functions
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

// Set up the content width value based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 960;
}

/* Load theme functions */
require dirname( __FILE__ ) . '/src/php/nieuw-functions.php';

/* Autoload theme PHP classes */
spl_autoload_register( 'nieuw_autoloader' );

/* Register actions */
add_action( 'after_setup_theme', 'Nieuw_Settings::setup' );
add_action( 'after_switch_theme', 'Nieuw_Settings::after_switch_theme' );
add_action( 'customize_register', 'Nieuw_Customizer::register' );
add_action( 'widgets_init', 'Nieuw_Widgets::sidebars' );
add_action( 'wp_enqueue_scripts', 'Nieuw_Frontend::enqueue_scripts' );
add_action( 'wp_head', 'Nieuw_Frontend::resource_hints', 7, 0 );

/* Register filters */
add_filter( 'big_image_size_threshold', 'Nieuw_Settings::big_image_size_threshold' );
add_filter( 'body_class', 'Nieuw_Frontend::body_class' );
add_filter( 'excerpt_more', 'Nieuw_Frontend::more' );
add_filter( 'get_the_archive_title_prefix', 'Nieuw_Frontend::archive_title_prefix' );
add_filter( 'should_load_separate_core_block_assets', '__return_true' );
add_filter( 'wp_calculate_image_sizes', 'Nieuw_Frontend::wp_calculate_image_sizes' );
add_filter( 'wp_editor_set_quality', 'Nieuw_Settings::wp_editor_set_quality' );
add_filter( 'wp_lazy_loading_enabled', 'Nieuw_Frontend::wp_lazy_loading_enabled', 10, 3 );
