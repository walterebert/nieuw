<?php
/**
 * Feature image
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

if ( has_post_thumbnail() ) :
	?>
	<div class="entry-image u-photo">
		<?php
		if ( is_home() ) {
			$thumbnail = get_the_post_thumbnail( null, 'medium' );
			$thumbnail = preg_replace( '/(width|height)="380"/', '$1="190"', $thumbnail );
		} else {
			$thumbnail = get_the_post_thumbnail( null, 'full' );
		}
		echo $thumbnail;
		?>
	</div>
<?php endif; ?>
