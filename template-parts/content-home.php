<?php
/**
 * Home content
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

if ( ! is_front_page() ) :
	?>

	<div class="entry-title">
		<h1><?php echo single_post_title(); ?></h1>
	</div>

	<?php
endif;

if ( have_posts() ) {
	?>
	<div class="h-feed">
		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', 'excerpt' );
		endwhile;
		?>
	</div>
	<?php
	the_posts_navigation();

} else {

	get_template_part( 'template-parts/content', '404' );

}
