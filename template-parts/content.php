<?php
/**
 * Page content
 *
 * @package WordPress
 * @subpackage Nieuws
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

the_content();

wp_link_pages(
	array(
		'before' => '<div class="page-links">' . __( 'Pages:', 'nieuw' ),
		'after'  => '</div>',
	)
);
