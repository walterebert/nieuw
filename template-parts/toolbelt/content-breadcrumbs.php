<?php
/**
 * Toolbelt breadcrumb https://github.com/BinaryMoon/wp-toolbelt/wiki/Breadcrumbs
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

if ( function_exists( 'toolbelt_breadcrumbs' ) ) :
	?>
	<section class="container">
		<?php toolbelt_breadcrumbs(); ?>
	</section>
<?php endif; ?>
