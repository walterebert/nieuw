<?php
/**
 * No content available
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}
?>

<div class="entry-title">
	<h1><?php echo esc_html( 'Nothing Found', 'nieuw' ); ?></h1>
</div>

<div class="entry-content">
	<?php get_search_form(); ?>
</div>
