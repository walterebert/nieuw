<?php
/**
 * Excerpt content
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

if ( ! post_password_required() ) :
	?>
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'excerpt h-entry' ); ?>>
		<div class="entry-title">
			<h2 id="post-<?php the_ID(); ?>-title"><a href="<?php echo esc_url( get_permalink() ); ?>" class="p-name u-url" rel="bookmark"><?php echo get_the_title(); ?></a></h2>
		</div>
		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<time class="entry-date dt-published" datetime="<?php echo esc_attr( get_the_date( DATE_W3C ) ); ?>">
				<?php echo esc_html( get_the_date() ); ?>
			</time>
		</div>
		<?php endif; ?>
		<?php
		if ( ! is_search() ) {
			get_template_part( 'template-parts/content', 'thumbnail' );
		}
		?>
		<div class="entry-content p-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
<?php endif; ?>
