<?php
/**
 * Archive page.
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

get_header();

get_template_part( 'template-parts/toolbelt/content', 'breadcrumbs' );
?>
	<?php the_archive_title( '<div class="entry-title"><h1>', '</h1></div>' ); ?>

	<?php if ( have_posts() ) : ?>

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'excerpt' );
			endwhile;
			?>

			<?php the_posts_navigation(); ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/content', '404' ); ?>

	<?php endif; ?>

<?php
get_footer();
