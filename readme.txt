=== Nieuw ===
Contributors: walterebert

== Description ==

A WordPress blogging theme that puts your content first. Everything else is at the bottom.

Features:

- Designed for reading.
- Written with accessibility in mind.
- Fast loading pages.
- With "Dark Mode" support, using the settings of your device.
- Progressively enhanced. This means the theme will function in old browsers (possibly with a fallback).

The theme has built-in support for the following plugins:

- [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
- [Toolbelt](https://wordpress.org/plugins/wp-toolbelt/)

Included webfonts:

- [Bitter Pro](https://github.com/solmatas/BitterPro)
- [Exo 2](https://github.com/NDISCOVER/Exo-2.0)
- [Source Code Pro](https://github.com/adobe-fonts/source-code-pro)

These fonts are licensed under the [SIL Open Font License](https://scripts.sil.org/OFL) and included as [variable fonts](https://variablefonts.io/). The webfonts are not loaded by default. They need to be activated in the theme options.

= Theme options =

You can change the appearance of the theme using the WordPress Customizer.

A color palette can be selected in the "Colors" section.

The following features can be changed in the "Nieuw Settings" section:

- Hide navigation button.
- Activate featured images.
- Activate dark mode support.
- Use webfonts.

= Extending the Nieuw theme =

[CSS custom properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties) are used in the theme stylesheet. These can be found at the start of the style.css file. You can use these to change, for example, colours or fonts as custom CSS.

<pre><code>
:root {
    --font-content: "Comic Sans MS", sans-serif;
    --font-default: "Impact", sans-serif;
    --text-color: chocolate;
}
</code></pre>

The following pluggable functions can be defined in a [child theme](https://developer.wordpress.org/themes/advanced-topics/child-themes/) to overwrite its functionality them.

<code>nieuw_copyright()</code> changes the HTML for the copyright notice at the bottom of the page.

<code>nieuw_author()</code> changes the HTML of the author name in blog posts.

<code>nieuw_entry_info()</code> changes the HTML of how categories and tags are displayed below a blog post.

<code>nieuw_entry_meta()</code> changes the HTML of blog post meta information, like author and publishing date.

<code>nieuw_html_class()</code> adds a class attribute to the &lt;html&gt; element.

<code>nieuw_navigation_link()</code> changes the HTML of the navigation icon at the top of the page.

<code>nieuw_next_previous_post()</code> changes the HTML of of the next / previous post navigation below a blog post.

<code>nieuw_search_dialog()</code> changes the HTML of the search dialog/modal.

To remove one of the above functions you can define an empty function, e.g.:

<pre><code>
function nieuw_author() {
    /* Do nothing */
}
</code></pre>

== Screenshots ==

1. Page example
2. Page in dark mode
3. Blogpost with feature image
4. Blogpost with code example
5. Blogposts overview page
6. Contact Form 7 example
7. Navigation menu is up
8. Select a colour palette
