<?php
/**
 * Page footer
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}
?>

</main><!-- #content -->

	<footer id="site-footer">
		<div class="container">
			<section id="site-navigation-container">
				<h2 id="site-navigation-heading" class="screen-reader-text"><?php echo esc_html__( 'Site navigation', 'nieuw' ); ?></h2>
				<?php if ( has_nav_menu( 'navigation' ) ) : ?>
				<nav id="site-navigation" tabindex="-1">
					<?php
					the_custom_logo();

					wp_nav_menu(
						array(
							'theme_location' => 'navigation',
							'menu_id'        => 'navigation-menu',
							'menu_class'     => 'nav-menu',
						)
					);
					?>
				</nav>
				<?php endif; ?>

				<?php if ( empty( get_theme_mod( 'hide-search' ) ) ): ?>
				<p>
				<?php $search_text = __( 'Search', 'nieuw' ); ?>
				<a href="<?php echo esc_url( home_url( '/?s=' ) ); ?>" class="site-search-toggle" aria-label="<?php echo esc_attr( $search_text ); ?>" title="<?php echo esc_attr( $search_text ); ?>">
					<svg width="32" height="32" viewBox="0 0 20 20">
						<path d="M12.9 14.32c-1.34 1.049-3.050 1.682-4.908 1.682-4.418 0-8-3.582-8-8s3.582-8 8-8c4.418 0 8 3.582 8 8 0 1.858-0.633 3.567-1.695 4.925l0.013-0.018 5.35 5.33-1.42 1.42-5.33-5.34zM8 14c3.314 0 6-2.686 6-6s-2.686-6-6-6v0c-3.314 0-6 2.686-6 6s2.686 6 6 6v0z"></path>
					</svg>
				</a>
				<?php endif; ?>

				<?php if ( empty( get_theme_mod( 'hide-feed' ) ) ): ?>
				<p>
				<?php $rss_feed_text = __( 'RSS feed', 'nieuw' ); ?>
				<a href="<?php echo esc_url( get_bloginfo( 'rss2_url' ) ); ?>" class="site-feed" aria-label="<?php echo esc_attr( $rss_feed_text ); ?>" title="<?php echo esc_attr( $rss_feed_text ); ?>">
					<svg width="32" height="32" viewBox="0 0 32 32">
						<path d="M4.259 23.467c-2.35 0-4.259 1.917-4.259 4.252 0 2.349 1.909 4.244 4.259 4.244 2.358 0 4.265-1.895 4.265-4.244-0-2.336-1.907-4.252-4.265-4.252zM0.005 10.873v6.133c3.993 0 7.749 1.562 10.577 4.391 2.825 2.822 4.384 6.595 4.384 10.603h6.16c-0-11.651-9.478-21.127-21.121-21.127zM0.012 0v6.136c14.243 0 25.836 11.604 25.836 25.864h6.152c0-17.64-14.352-32-31.988-32z"></path>
					</svg>
				</a>
				<?php endif; ?>
			</section>
			<section>
				<h2 class="screen-reader-text"><?php echo __( 'Site info', 'nieuw' ); ?></h2>
				<?php
				nieuw_copyright();

				if ( is_active_sidebar( 'footer-sidebar' ) ) {
					dynamic_sidebar( 'footer-sidebar' );
				}

				if ( function_exists( 'toolbelt_social_menu' ) ) {
					toolbelt_social_menu();
				}
				?>
			</section>
		</div>
	</footer>

</div><!-- #page -->

<template id="site-search-template">
	<?php nieuw_search_dialog(); ?>
</template>

<?php
wp_footer();
