<?php
/**
 * Page.
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

get_header();

get_template_part( 'template-parts/toolbelt/content', 'breadcrumbs' );

if ( have_posts() ) :

	while ( have_posts() ) :
		the_post();
		?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( 'h-entry' ); ?>>
			<?php if ( ! post_password_required() && has_post_thumbnail() ) : ?>
				<div class="entry-postimage u-photo">
					<?php the_post_thumbnail(); ?>
				</div>
			<?php endif; ?>
			<div  class="entry-title">
				<h1 class="p-name"><?php echo get_the_title(); ?></h1>
			</div>

			<div class="entry-content e-content">
				<?php the_content(); ?>
			</div>

			<?php
			wp_link_pages();

			comments_template( '', true );
			?>
		</article>
		<?php
	endwhile;

endif;

get_footer();
