<?php
/**
 * The template for displaying comments
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password,
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

$comments_count = get_comments_number();
$comments_open = comments_open();
if ( $comments_open || ( ! $comments_open && $comments_count ) ) :
	?>

	<div class="container comments-container">
		<details id="comments-details">
			<summary id="comments-summary" data-count="<?php echo absint( $comments_count ); ?>">
				<span id="comments-no-toggle">
					<?php echo esc_html__( 'Comments', 'nieuw' ); ?>
				</span>
				<span id="comments-open-null" hidden>
					<?php echo esc_html__( 'Add a comment', 'nieuw' ); ?>
				</span>
				<span id="comments-open" hidden>
					<?php
					/* translators: %d: total number of comments */
					printf( esc_html__( 'Show comments (%d)', 'nieuw' ), absint( $comments_count ) );
					?>
				</span>
				<span id="comments-close-null" hidden>
					<?php echo esc_html__( 'Hide comment form', 'nieuw' ); ?>
				</span>
				<span id="comments-close" hidden>
					<?php
					printf( esc_html__( 'Hide comments', 'nieuw' ) );
					?>
				</span>
			</summary>
			<div id="comments" class="comments-area <?php echo get_option( 'show_avatars' ) ? 'show-avatars' : ''; ?>">
				<?php if ( have_comments() ) : ?>
					<ol class="comment-list">
						<?php wp_list_comments(); ?>
					</ol><!-- .comment-list -->

					<?php the_comments_pagination(); ?>
				<?php endif; ?>

				<?php
				if ( $comments_open ) {
					comment_form();
				}
				?>
			</div>
		</details>
	</div>

<?php endif; ?>
