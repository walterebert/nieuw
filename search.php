<?php
/**
 * Archive page.
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

get_header();

get_template_part( 'template-parts/toolbelt/content', 'breadcrumbs' );

$search_query = '';
if ( isset( $_GET['s'] ) ) {
	$search_query = trim( filter_input( INPUT_GET, 's' ) );
}

$search_count = 0;
if ( '' !== $search_query && have_posts() ) {
	$search_count = $wp_query->found_posts;
}
?>

	<div class="entry-title">
		<h1>
		<?php
		/* translators: %d: total number of search results */
		echo esc_html( sprintf( __( 'Search results (%d)', 'nieuw' ), $search_count ) );
		?>
		</h1>
	</div>

	<div class="entry-content">
		<?php get_search_form(); ?>
	</div>

	<?php
	if ( $search_count ) :
		?>
			<?php
			while ( have_posts() ) {
				the_post();

				get_template_part( 'template-parts/content', 'excerpt' );
			}
			?>

			<?php
			the_posts_navigation(
				array(
					'prev_text' => __( 'Previous', 'nieuw' ),
					'next_text' => __( 'Next', 'nieuw' ),
				)
			);
			?>
	<?php else : ?>
		<article class="entry-content">
			<h2><?php echo esc_html( 'Nothing Found', 'nieuw' ); ?></h2>
		</article>
	<?php endif; ?>

<?php
get_footer();
