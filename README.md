# Nieuw

A WordPress blogging theme that puts your content first. Everything else is at the bottom.

**Not yet ready for use on live sites. Before you can use this theme, you have to install all dependencies and run a build script. Node.js 16 or later are required to do this.**

```
npm install
npm run build
```

Features:

- Designed for reading.
- Written with accessibility in mind.
- Fast loading pages.
- With "Dark Mode" support, using the device settings of a website visitor.
- Progressively enhanced. This means the theme will function in old browsers (possibly with fallback functionality).

The theme has built-in support for the following plugins:

- [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
- [Toolbelt](https://wordpress.org/plugins/wp-toolbelt/)

Included webfonts:

- [Bitter Pro](https://github.com/solmatas/BitterPro)
- [Exo 2](https://github.com/NDISCOVER/Exo-2.0)
- [Source Code Pro](https://github.com/adobe-fonts/source-code-pro)

These fonts are licensed under the [SIL Open Font License](https://scripts.sil.org/OFL) and included as [variable fonts](https://variablefonts.io/). The webfonts are not loaded by default. They need to be activated in the theme options.

## Theme options ##

You can change the appearance of the theme using the WordPress Customizer.

A color palette can be selected in the "Colors" section.

The following features can be changed in the "Nieuw Settings" section:

- Hide navigation button.
- Activate featured images.
- Activate dark mode support.
- Use webfonts.

## Extending the Nieuw theme ##

[CSS custom properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties) are used in the theme stylesheet. These can be found at the start of the style.css file. You can use these to change, for example, colours or fonts under "Additional CSS" in the WordPress Customizer.

```
:root {
    --font-content: "Comic Sans MS", sans-serif;
    --font-default: "Impact", sans-serif;
    --text-color: chocolate;
}
```

The following pluggable functions can be defined in a [child theme](https://developer.wordpress.org/themes/advanced-topics/child-themes/) to overwrite them.

`nieuw_copyright()` changes the HTML for the copyright notice at the bottom of the page.

`nieuw_author()` changes the HTML of the author name in blog posts.

`nieuw_entry_info()` changes the HTML of how categories and tags are displayed below a blog post.

`nieuw_entry_meta()` changes the HTML of blog post meta information, like author and publishing date.

`nieuw_html_class()` adds a class attribute to the &lt;html&gt; element.

`nieuw_navigation_link()` changes the HTML of the navigation icon at the top of the page.

`nieuw_next_previous_post()` changes the HTML of of the next / previous post navigation below a blog post.

`nieuw_search_dialog()` changes the HTML of the search dialog/modal.

To remove one of the above functions you can define an empty function, e.g.:

```
<?php
function nieuw_author() {
    /* Do nothing */
}
?>
```

## Screenshots

![Page example](assets/screenshot-1.png "Page example")

![Page in dark mode](assets/screenshot-2.png "Page in dark mode")

![Blogpost example](assets/screenshot-3.png "Blogpost with feature image")

![Blogpost with feature image](assets/screenshot-4.png "Blogpost with code example")

![Blogposts overview page](assets/screenshot-5.png "Blogposts overview page")

![Contact Form 7 example](assets/screenshot-6.png "Contact Form 7 example")

![Navigation menu is up](assets/screenshot-7.png "Navigation menu is up")

![Select a colour palette](assets/screenshot-8.png "Select a colour palette")
