<?php
/**
 * Search form
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

$search_button_text = __( 'Search', 'nieuw' );
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo esc_html__( 'Search for:', 'nieuw' ); ?></span>
		<input type="search" class="search-field" value="<?php echo get_search_query(); ?>" name="s" required />
	</label>
	<button class="search-submit" title="<?php echo esc_attr( $search_button_text ); ?>"><?php echo esc_html( $search_button_text ); ?></button>
</form>
