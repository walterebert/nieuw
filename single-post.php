<?php
/**
 * Post.
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

get_header();

get_template_part( 'template-parts/toolbelt/content', 'breadcrumbs' );
?>

	<?php if ( have_posts() ) : ?>

			<?php
			while ( have_posts() ) :
				the_post();
				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'h-entry' ); ?>>
					<?php get_template_part( 'template-parts/content', 'thumbnail' ); ?>

					<div  class="entry-title">
						<h1 class="p-name"><?php echo get_the_title(); ?></h1>
					</div>

					<?php nieuw_entry_meta(); ?>

					<div class="entry-content e-content">
						<?php get_template_part( 'template-parts/content' ); ?>
					</div>

					<?php comments_template( '', true ); ?>

					<div class="entry-info"><?php nieuw_entry_info(); ?></div>
				</article>

				<?php nieuw_next_previous_post(); ?>

			<?php endwhile; ?>

	<?php endif; ?>

<?php
get_footer();
