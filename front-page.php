<?php
/**
 * Front page.
 *
 * @package WordPress
 * @subpackage Nieuw
 */

 // Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

get_header();

if ( is_home() ) {
	get_template_part( 'template-parts/content', 'home' );
} else {
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>

			<?php
		}
	}
}

get_footer();
