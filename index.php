<?php
/**
 * Default page.
 *
 * @package WordPress
 * @subpackage Nieuw
 */

// Deny direct access.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

get_header();

get_template_part( 'template-parts/toolbelt/content', 'breadcrumbs' );
?>
	
	<?php if ( have_posts() ) : ?>

			<?php
			while ( have_posts() ) :
				the_post();
				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-title">
						<h2><a href="<?php echo esc_url( get_permalink() ); ?>" class="u-url p-name" rel="bookmark"><?php echo get_the_title(); ?></a></h2>
					</div>

					<div class="entry-content e-content">
					<?php the_content(); ?>

					<?php
					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'nieuw' ),
							'after'  => '</div>',
						)
					);
					?>
					</div><!-- .entry-content -->
				</article><!-- #post-## -->

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/content', '404' ); ?>

	<?php endif; ?>

<?php
get_footer();
