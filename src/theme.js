/**
 * Nieuw theme JS
 */

/* Only run JS in supported browsers */
if (
  'function' === typeof NodeList.prototype.forEach &&
  'content' in document.createElement('template') &&
  'open' in document.createElement('details')
) {
  (function (nieuw) {
    'use strict';

    var commentsDetails = document.getElementById('comments-details');
    var content = document.getElementById('content');
    var html = document.documentElement;
    var page = document.getElementById('page');
    var navlink = document.getElementById('navlink');
    var navlinkText = {};
    if (navlink) {
      navlinkText = navlink.querySelector('.screen-reader-text');
    }
    var contentWidth = content.clientWidth;
    var direction = window.getComputedStyle(content).direction;
    var footerContainer = document.querySelector('#site-footer > .container');
    var navOpening = false;

    /**
     * Make code blocks with scrollbars focusable
     */
    function focusableCodeBlocks() {
      content.querySelectorAll('.wp-block-code').forEach(function (element) {
        element.removeAttribute('tabindex');
        if (element.scrollWidth > content.clientWidth) {
          element.setAttribute('tabindex', '0');
        }
      });
    }

    function scrollbarPlaceholder(width) {
      var newWidth = 0;
      if (width) {
        newWidth = parseInt(width, 10);
      }

      if ('rtl' === direction) {
        content.style.marginLeft = newWidth + 'px';
        navlink.style.marginLeft = newWidth + 'px';
      } else {
        content.style.marginRight = newWidth + 'px';
        navlink.style.marginRight = newWidth + 'px';
      }
    }

    /**
     * Close site navigation
     */
    function navigationClose() {
      navOpening = false;
      content.removeAttribute('inert');

      if (!navlink) {
        return;
      }

      scrollbarPlaceholder();
      navlink.setAttribute('aria-expanded', 'false');
      navlink.setAttribute('title', nieuw.navlink.titleClosed);
      navlinkText.innerText = nieuw.navlink.titleClosed;

      if (html.classList.contains('site-navigation-open')) {
        footerContainer.style.visibility = 'hidden';
        html.classList.add('site-navigation-close');
        html.classList.remove('site-navigation-open');
        window.setTimeout(function () {
          footerContainer.style.visibility = 'visible';
          html.classList.remove('site-navigation-close');
        }, 300);
      }
    }

    /**
     * Open navigation menu
     */
    function navigationOpen() {
      if (navlink) {
        navOpening = true; // Prevent double clicks.

        html.classList.add('site-navigation-open');

        // Prevent layout shift from removing the scroll bar.
        scrollbarPlaceholder(content.clientWidth - contentWidth);
      }
    }

    /**
     * Set states after navigation has opened
     */
    function navigationOpenAfter() {
      if (navlink) {
        content.setAttribute('inert', '');
        navOpening = false;
        navlink.setAttribute('aria-expanded', 'true');
        navlink.setAttribute('title', nieuw.navlink.titleOpened);
        navlinkText.innerText = nieuw.navlink.titleOpened;
      }
    }

    /**
     * Open comments section
     */
    function openComments() {
      if (window.location.hash.match(/^#comment\-/) && commentsDetails) {
        commentsDetails.open = true;
      }
    }

    /**
     * Close search dialog
     */
    function searchClose() {
      html.classList.remove('search-open');
      var searchDialog = document.getElementById('site-search');
      page.removeAttribute('inert');
      if (searchDialog) {
        searchDialog.parentNode.removeChild(searchDialog);
      }
    }

    /**
     * Open search dialog
     */
    function searchOpen() {
      var searchDialog = document.getElementById('site-search-template').content.cloneNode(true);
      var searchInput = searchDialog.querySelector('input[type=search]');
      document.body.appendChild(searchDialog);

      searchInput.value = '';
      searchInput.focus();
      html.classList.add('search-open');
      page.setAttribute('inert', '');

      // Close search dialog button.
      document
        .getElementById('site-search-close')
        .addEventListener('click', function () {
          searchClose();
        });
    }

    /**
     * Set site navigation trigger
     */
    if (navlink) {
      navlink.setAttribute('role', 'button');
      navlink.setAttribute('aria-haspopup', 'menu');
      navlink.setAttribute('aria-expanded', 'false');
      navlink.addEventListener('click', function (event) {
        event.preventDefault();

        if (false === navOpening && html.classList.contains('site-navigation-open')) {
          navigationClose();
        } else {
          navigationOpen();

          if (window.matchMedia('(prefers-reduced-motion: reduced)').matches) {
            navigationOpenAfter();
          } else {
            window.setTimeout(function () {
              navigationOpenAfter();
            }, 300);
          }
        }
      });
    }

    /* Enable search toggles */
    document.querySelectorAll('.site-search-toggle').forEach(function (element) {
      element.setAttribute('role', 'button');
      element.setAttribute('aria-haspopup', 'dialog');
      element.addEventListener('click', function (event) {
        event.preventDefault();
        searchOpen();
      });
    });

    /* Pause autoplaying videos when reduced motion is active */
    if (window.matchMedia('(prefers-reduced-motion: reduced)').matches) {
      document.querySelectorAll('video').forEach(function (element) {
        element.pause();
      });
    }

    /* Toggle comments section */
    if (commentsDetails) {
      var commentsSummary = document.getElementById('comments-summary');
      var commentsClose = document.getElementById('comments-close');
      var commentsCloseNull = document.getElementById('comments-close-null');
      var commentsOpen = document.getElementById('comments-open');
      var commentsOpenNull = document.getElementById('comments-open-null');
      var commentsFallback = document.getElementById('comments-no-toggle');
      commentsFallback.parentNode.removeChild(commentsFallback);
      if (commentsSummary.dataset.count > 0) {
        commentsOpen.hidden = false;
      } else {
        commentsOpenNull.hidden = false;
      }
      commentsDetails.addEventListener('toggle', function () {
        if (this.open) {
          commentsOpen.hidden = true;
          commentsOpenNull.hidden = true;
          if (commentsSummary.dataset.count > 0) {
            commentsClose.hidden = false;
          } else {
            commentsCloseNull.hidden = false;
          }
        } else {
          if (commentsSummary.dataset.count > 0) {
            commentsOpen.hidden = false;
            commentsOpenNull.hidden = true;
          } else {
            commentsOpen.hidden = true;
            commentsOpenNull.hidden = false;
          }
          commentsClose.hidden = true;
          commentsCloseNull.hidden = true;
        }
      });
    }

    /* Actions when pressing a key */
    window.addEventListener('keydown', function (event) {
      /* The escape key */
      if ('Escape' === event.key) {
        searchClose();
        if (content.hasAttribute('inert')) {
          navigationClose();
        }
      }
    });

    /* Check URL on navigation */
    window.addEventListener('popstate', function () {
      openComments();
    });

    /* Make code blocks with scrollbars focusable on window resize */
    window.addEventListener('resize', function () {
      focusableCodeBlocks();
    });

    /* Set initial navigation stat */
    if (navlink) {
      navigationClose();
    }

    /* Check comments */
    openComments();

    /* Check code blocks */
    focusableCodeBlocks();
  })(nieuw);
}
