<?php
/**
 * Palette Customizer control
 *
 * @package WordPress
 * @subpackage Nieuw
 */

/**
 * Custom palette control class
 */
class Nieuw_Palette_Customize_Control extends WP_Customize_Control {
	/**
	 * Render control content
	 */
	public function render_content() {
		$input_id         = '_customize-input-' . $this->id;
		$description_id   = '_customize-description-' . $this->id;
		$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';
 
		if ( empty( $this->choices ) ) {
			return;
		}

		$img_uri_base = get_stylesheet_directory_uri() . '/assets/palette/';

		$name = '_customize-radio-' . $this->id;
		if ( empty( $this->choices ) ) {
			return;
		}

		$name = '_customize-radio-' . $this->id;
		?>

		<style>
		.nieuw-palette-image {border: 1px solid #dcdcde;}
		</style>

		<?php if ( ! empty( $this->label ) ) : ?>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
		<?php endif; ?>
		<?php if ( ! empty( $this->description ) ) : ?>
			<span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
		<?php endif; ?>

		<?php foreach ( $this->choices as $value => $label ) : ?>
			<span class="customize-inside-control-row">
				<input
					id="<?php echo esc_attr( $input_id . '-radio-' . $value ); ?>"
					type="radio"
					<?php echo $describedby_attr; ?>
					value="<?php echo esc_attr( $value ); ?>"
					name="<?php echo esc_attr( $name ); ?>"
					<?php $this->link(); ?>
					<?php checked( $this->value(), $value ); ?>
					/>
				<label for="<?php echo esc_attr( $input_id . '-radio-' . $value ); ?>">
					<?php if ( 'none' === $value ) : ?>
						<?php echo esc_html( $label ); ?>
					<?php else : ?>
						<img src="<?php echo esc_url( $img_uri_base . $value . '.png' ); ?>" alt="<?php echo esc_attr( $label ); ?>" height="20" width="100" class="nieuw-palette-image" />
					<?php endif; ?>
				</label>
			</span>
		<?php endforeach; ?>

		<?php
	}
}
