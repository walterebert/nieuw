<?php
/**
 * Nieuw functions
 *
 * @package WordPress
 * @subpackage Nieuw
 */

/**
 * PHP autoloader
 *
 * @param string $class Class name.
 */
function nieuw_autoloader( $class ) {
	if ( 'Nieuw_' === substr( $class, 0, 6 ) ) {
		$file = dirname( __FILE__ ) . '/class-' . str_replace( '_', '-', strtolower( $class ) ) . '.php';
		if ( file_exists( $file ) ) {
			require_once $file;
		}
	}
}

if ( ! function_exists( 'nieuw_author' ) ) {
	/**
	 * Show blog post author information
	 */
	function nieuw_author() {
		$author = get_the_author();
		if ( $author ) {
			$author_url = get_the_author_meta( 'user_url' );
			if ( ! $author_url ) {
				$author_url = home_url( '/' );
			}
			?>
			<li>
				<a href="<?php echo esc_url( $author_url ); ?>" class="entry-author p-author h-card">
					<?php echo esc_html( get_the_author() ); ?>
				</a>
			</li>
			<?php
		}
	}
}

if ( ! function_exists( 'nieuw_copyright' ) ) {
	/**
	 * Copyright notice
	 */
	function nieuw_copyright() {
		$copyright = get_theme_mod( 'copyright' );
		if ( ! $copyright ) {
			$copyright = '&copy; <a href="' . esc_url( home_url( '/' ) ) . '">' . esc_html( get_bloginfo( 'name' ) ) . '</a>';
		}

		echo '<p>' . $copyright . PHP_EOL;
	}
}

if ( ! function_exists( 'nieuw_entry_info' ) ) {
	/**
	 * Entry info for singular posts
	 */
	function nieuw_entry_info() {
		$categories_list = get_the_category_list( ' ' );

		// Only display linked categories.
		if ( strpos( $categories_list, '</a>' ) ) {
			// Add micro format info.
			echo preg_replace(
				'%<a href="([^"]+)">(.*)</a>%muU',
				'<a href="$1" rel="category" class="p-category">$2</a>',
				wp_kses( $categories_list, 'data' )
			);

			echo ' ';
		}

		$tags_list = get_the_tag_list( '', ' ' );

		// Add micro format info.
		echo preg_replace(
			'%<a href="([^"]+)">(.*)</a>%muU',
			'<a href="$1" rel="tag" class="p-category">$2</a>',
			wp_kses( $tags_list, 'data' )
		);
	}
}

if ( ! function_exists( 'nieuw_entry_meta' ) ) {
	/**
	 * HTML of blog post meta information, like author and publishing date.
	 */
	function nieuw_entry_meta() {
		?>
		<div class="entry-meta">
			<ul>
				<?php nieuw_author(); ?>
				<li>
					<time class="entry-date dt-published" datetime="<?php echo esc_attr( get_the_date( DATE_W3C ) ); ?>">
						<?php echo esc_html( get_the_date() ); ?>
					</time>
				</li>
			</ul>
		</div>
		<?php
	}
}

if ( ! function_exists( 'nieuw_html_class' ) ) {
	/**
	 * CSS classes for the HTML element
	 */
	function nieuw_html_class() {
		// Get theme mod.
		$class = get_theme_mod( 'palette' );

		if ( $class ) {
			echo ' class="palette-' . esc_attr( $class ) . '"';
		}
	}
}

if ( ! function_exists( 'nieuw_navigation_link' ) ) {
	/**
	 * Navigation link
	 */
	function nieuw_navigation_link() {
		$hide = get_theme_mod( 'hide_navlink' );
		if ( ! $hide ) :
			?>
			<header id="site-header">
				<a href="#site-navigation" id="navlink" title="<?php echo esc_attr__( 'Go to navigation', 'nieuw' ); ?>">
					<span class="screen-reader-text"><?php echo esc_html__( 'Go to navigation', 'nieuw' ); ?></span>
				</a>
			</header>
			<?php
		endif;
	}
}

if ( ! function_exists( 'nieuw_next_previous' ) ) {
	/**
	 * Next and previous post links
	 */
	function nieuw_next_previous_post() {
		?>
		<div class="container">
			<nav class="nav-single" aria-label="<?php echo esc_attr__( 'Post navigation', 'nieuw' ); ?>">
				<?php
				$prevous_post_link = get_previous_post_link( '%link', '%title' );
				if ( $prevous_post_link ) :
					?>
					<span class="nav-previous">
						<?php echo $prevous_post_link; ?>
						<span class="meta-nav"><?php echo __( 'Previous post', 'nieuw' ); ?></span>
					</span>
					<?php
				endif;
				$next_post_link = get_next_post_link( '%link', '%title' );
				if ( $next_post_link ) :
					?>
					<span class="nav-next">
						<?php echo $next_post_link; ?>
						<span class="meta-nav"><?php echo __( 'Next post', 'nieuw' ); ?></span>
					</span>
				<?php endif; ?>
			</nav>
		</div>
		<?php
	}
}

if ( ! function_exists( 'nieuw_search_dialog' ) ) {
	/**
	 * Search dialog
	 */
	function nieuw_search_dialog() {
		$close_dialog_text = __( 'Close search dialog', 'nieuw' );
		?>
		<div id="site-search" role="dialog">
			<?php get_search_form(); ?>

			<button type="button" id="site-search-close" aria-label="<?php echo esc_attr( $close_dialog_text ); ?>" title="<?php echo esc_attr( $close_dialog_text ); ?>">
				<svg height="60" width="60" viewBox="0 0 20 20">
					<path d="M10 8.586l-7.071-7.071-1.414 1.414 7.071 7.071-7.071 7.071 1.414 1.414 7.071-7.071 7.071 7.071 1.414-1.414-7.071-7.071 7.071-7.071-1.414-1.414-7.071 7.071z"></path>
				</svg>
			</button>
		</div>
		<?php
	}
}
