<?php
/**
 * Theme settings
 *
 * @package WordPress
 * @subpackage Nieuw
 */

/**
 * Theme backend class
 */
class Nieuw_Settings {
	/**
	 * Customise WordPress settings when activating the theme
	 */
	public static function after_switch_theme() {
		update_option( 'thumbnail_size_w', 190 );
		update_option( 'thumbnail_size_h', 190 );
		update_option( 'thumbnail_crop', 1 );

		update_option( 'medium_size_w', 380 );
		update_option( 'medium_size_h', 380 );
		update_option( 'medium_crop', 1 );
	}

	/**
	 * Customise maximum image size
	 */
	public static function big_image_size_threshold() {
		return 1920;
	}

	/**
	 * List of used blocks
	 */
	public static function get_blocks() {
		return array(
			'button',
			'calendar',
			'code',
			'columns',
			'cover',
			'gallery',
			'latest-posts',
			'search',
		);
	}

	/**
	 * Set colour palette
	 */
	public static function palette_colors() {
		$palette = get_theme_mod( 'palette' );

		switch ( $palette ) {
			case 'blue':
				$colors = array(
					'name'   => 'blue',
					'colors' => array(
						array(
							'name'  => esc_attr__( 'Dark gray', 'nieuw' ),
							'slug'  => 'dark-gray',
							'color' => '#333333',
						),
						array(
							'name'  => esc_attr__( 'Blue', 'nieuw' ),
							'slug'  => 'blue',
							'color' => '#3366AA',
						),
						array(
							'name'  => esc_attr__( 'Light blue', 'nieuw' ),
							'slug'  => 'light-blue',
							'color' => '#88BBFF',
						),
						array(
							'name'  => esc_attr__( 'Extra light blue', 'nieuw' ),
							'slug'  => 'extra-light-blue',
							'color' => '#EEEEFF',
						),
						array(
							'name'  => esc_attr__( 'Brown', 'nieuw' ),
							'slug'  => 'brown',
							'color' => '#996622',
						),
						array(
							'name'  => esc_attr__( 'White', 'nieuw' ),
							'slug'  => 'white',
							'color' => '#FFFFFF',
						),
					),
				);
				break;

			case 'green':
				$colors = array(
					'name'   => 'green',
					'colors' => array(
						array(
							'name'  => esc_attr__( 'Dark gray', 'nieuw' ),
							'slug'  => 'dark-gray',
							'color' => '#333333',
						),
						array(
							'name'  => esc_attr__( 'Green', 'nieuw' ),
							'slug'  => 'green',
							'color' => '#008800',
						),
						array(
							'name'  => esc_attr__( 'Light Green', 'nieuw' ),
							'slug'  => 'light-green',
							'color' => '#DDFFCC',
						),
						array(
							'name'  => esc_attr__( 'Brown', 'nieuw' ),
							'slug'  => 'brown',
							'color' => '#775500',
						),
						array(
							'name'  => esc_attr__( 'Beige', 'nieuw' ),
							'slug'  => 'beige',
							'color' => '#FFEEDD',
						),
						array(
							'name'  => esc_attr__( 'White', 'nieuw' ),
							'slug'  => 'white',
							'color' => '#FFFFFF',
						),
					),
				);
				break;

			case 'orange':
				$colors = array(
					'name'   => 'orange',
					'colors' => array(
						array(
							'name'  => esc_attr__( 'Dark gray', 'nieuw' ),
							'slug'  => 'dark-gray',
							'color' => '#333333',
						),
						array(
							'name'  => esc_attr__( 'Dark orange', 'nieuw' ),
							'slug'  => 'dark-orange',
							'color' => '#CC4400',
						),
						array(
							'name'  => esc_attr__( 'Orange', 'nieuw' ),
							'slug'  => 'orange',
							'color' => '#FF7700',
						),
						array(
							'name'  => esc_attr__( 'Extra light orange', 'nieuw' ),
							'slug'  => 'extra-light-orange',
							'color' => '#FFEEBB',
						),
						array(
							'name'  => esc_attr__( 'Cyan', 'nieuw' ),
							'slug'  => 'cyan',
							'color' => '#117799',
						),
						array(
							'name'  => esc_attr__( 'White', 'nieuw' ),
							'slug'  => 'white',
							'color' => '#FFFFFF',
						),
					),
				);
				break;

			case 'purple':
				$colors = array(
					'name'   => 'purple',
					'colors' => array(
						array(
							'name'  => esc_attr__( 'Dark gray', 'nieuw' ),
							'slug'  => 'dark-gray',
							'color' => '#333333',
						),
						array(
							'name'  => esc_attr__( 'Beige', 'nieuw' ),
							'slug'  => 'beige',
							'color' => '#FFCC77',
						),
						array(
							'name'  => esc_attr__( 'Purple', 'nieuw' ),
							'slug'  => 'purple',
							'color' => '#663399',
						),
						array(
							'name'  => esc_attr__( 'Light purple', 'nieuw' ),
							'slug'  => 'light-purple',
							'color' => '#BBAAFF',
						),
						array(
							'name'  => esc_attr__( 'Green', 'nieuw' ),
							'slug'  => 'green',
							'color' => '#117733',
						),
						array(
							'name'  => esc_attr__( 'White', 'nieuw' ),
							'slug'  => 'white',
							'color' => '#FFFFFF',
						),
					),
				);
				break;

			default:
				$colors = array(
					'name'   => 'default',
					'colors' => array(
						array(
							'name'  => esc_attr__( 'Dark gray', 'nieuw' ),
							'slug'  => 'dark-gray',
							'color' => '#333333',
						),
						array(
							'name'  => esc_attr__( 'Gray', 'nieuw' ),
							'slug'  => 'gray',
							'color' => '#555555',
						),
						array(
							'name'  => esc_attr__( 'Light gray', 'nieuw' ),
							'slug'  => 'light-gray',
							'color' => '#EEEEEE',
						),
						array(
							'name'  => esc_attr__( 'Blue', 'nieuw' ),
							'slug'  => 'blue',
							'color' => '#005588',
						),
						array(
							'name'  => esc_attr__( 'Red', 'nieuw' ),
							'slug'  => 'red',
							'color' => '#CC0000',
						),
						array(
							'name'  => esc_attr__( 'Yellow', 'nieuw' ),
							'slug'  => 'yellow',
							'color' => '#EEEE55',
						),
						array(
							'name'  => esc_attr__( 'White', 'nieuw' ),
							'slug'  => 'white',
							'color' => '#FFFFFF',
						),
					),
				);
		}

		return $colors;
	}

	/**
	 * Theme setup
	 */
	public static function setup() {
		add_theme_support( 'align-wide' );

		if ( empty( get_theme_mod( 'hide-feed') ) ) {
			add_theme_support( 'automatic-feed-links' );
		}

		$custom_logo = array(
			'height'               => 200,
			'width'                => 200,
			'flex-height'          => true,
			'flex-width'           => false,
		);
		add_theme_support( 'custom-logo', $custom_logo );

		add_theme_support( 'custom-units', 'rem', 'em' );

		add_theme_support(
			'html5',
			array(
				'comment-list',
				'comment-form',
				'search-form',
				'gallery',
				'caption',
				'style',
				'script'
			)
		);

		add_theme_support( 'responsive-embeds' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'wp-block-styles' );

		load_theme_textdomain( 'nieuw', get_template_directory() . '/languages' );

		register_nav_menus(
			array(
				'navigation' => esc_attr__( 'Navigation', 'nieuw' ),
			)
		);

		// Check theme options.
		if ( get_theme_mod( 'post-thumbnails' ) ) {
			add_theme_support( 'post-thumbnails' );
		}

		if ( is_admin() ) {
			$base_path = 'dist';
			if (WP_DEBUG) {
				$base_path = 'src';
			}
			// Add editor styles.
			$palette_colors = self::palette_colors();
			add_theme_support(
				'editor-color-palette',
				$palette_colors['colors']
			);
			add_theme_support( 'editor-styles' );

			$add_editor_style = array( "$base_path/css/editor-style.css" );
			if ( get_theme_mod( 'webfonts' ) ) {
				$add_editor_style[] = "$base_path/css/webfonts.css";
				$add_editor_style[] = "$base_path/css/editor-style-webfonts.css";
			}
			$palette = sanitize_key( get_theme_mod( 'palette' ) );
			if ( $palette ) {
				$add_editor_style[] = "$base_path/css/editor-style-$palette.css";
			}
			add_editor_style( $add_editor_style );
		}
	}

	/**
	 * Customise default image quality
	 */
	public static function wp_editor_set_quality() {
		return 85;
	}
}
