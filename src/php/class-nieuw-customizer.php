<?php
/**
 * Customizer
 *
 * @package WordPress
 * @subpackage Nieuw
 */

/**
 * Customizer class
 */
class Nieuw_Customizer {
	/**
	 * Sanitize bolean values
	 *
	 * @param string $value HEX color value.
	 */
	public static function sanitize_boolean( $value ) {
		return (bool) $value;
	}

	/**
	 * Customizer settings
	 *
	 * @param  WP_Customize_Manager $wp_customize WP Customize Mananager object.
	 */
	public static function register( WP_Customize_Manager $wp_customize ) {
		$wp_customize->add_setting(
			'palette',
			array(
				'default'           => 'default',
				'type'              => 'theme_mod',
				'transport'         => 'refresh',
				'sanitize_callback' => 'sanitize_key',
			)
		);

		$wp_customize->add_control(
			new Nieuw_Palette_Customize_Control(
				$wp_customize,
				'palette',
				array(
					'label'    => __( 'Palette', 'nieuw' ),
					'section'  => 'colors',
					'type'     => 'radio',
					'choices'  => array(
						'default' => __( 'Default', 'nieuw' ),
						'purple' => __( 'Prime purple', 'nieuw' ),
						'green' => __( 'Prime green', 'nieuw' ),
						'orange' => __( 'Prime orange', 'nieuw' ),
						'blue' => __( 'Prime blue', 'nieuw' ),
						'none' => __( 'None', 'nieuw' ),
					),
				)
			)
		);

		$wp_customize->add_section(
			'nieuw',
			array(
				'title' => __( 'Theme Options', 'nieuw' ),
			)
		);

		$wp_customize->add_setting(
			'hide_navlink',
			array(
				'type'      => 'theme_mod',
				'transport' => 'refresh',
				'sanitize_callback' => __CLASS__ . '::sanitize_boolean',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'hide_navlink',
				array(
					'label'       => __( 'Hide navigation button', 'nieuw' ),
					'description' => __( 'Removes the navigation button from the top of the page.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'hide_navlink',
					'type'        => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting(
			'post-thumbnails',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'postMessage',
				'sanitize_callback' => __CLASS__ . '::sanitize_boolean',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'post-thumbnails',
				array(
					'label'       => __( 'Activate featured images', 'nieuw' ),
					'description' => __( 'Enables the possibility to add a featured image to posts and pages.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'post-thumbnails',
					'type'        => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting(
			'dark-mode',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'postMessage',
				'sanitize_callback' => __CLASS__ . '::sanitize_boolean',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'dark-mode',
				array(
					'label'       => __( 'Activate dark mode support', 'nieuw' ),
					'description' => __( 'Automatically match the colors the system settings.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'dark-mode',
					'type'        => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting(
			'webfonts',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'refresh',
				'sanitize_callback' => __CLASS__ . '::sanitize_boolean',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'webfonts',
				array(
					'label'       => __( 'Use webfonts', 'nieuw' ),
					'description' => __( 'Load the fonts selected for this theme, instead of system fonts. The theme webfonts only contain <strong>latin</strong> glyphs.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'webfonts',
					'type'        => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting(
			'hide-search',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'refresh',
				'sanitize_callback' => __CLASS__ . '::sanitize_boolean',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'hide-search',
				array(
					'label'       => __( 'Hide search icon', 'nieuw' ),
					'description' => __( 'Hide the search icon in site footer.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'hide-search',
					'type'        => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting(
			'hide-feed',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'refresh',
				'sanitize_callback' => __CLASS__ . '::sanitize_boolean',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'hide-feed',
				array(
					'label'       => __( 'Hide feed icon', 'nieuw' ),
					'description' => __( 'Hide the RSS feed icon in the site footer.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'hide-feed',
					'type'        => 'checkbox',
				)
			)
		);

		$wp_customize->add_setting(
			'copyright',
			array(
				'type'              => 'theme_mod',
				'transport'         => 'refresh',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'copyright',
				array(
					'label'       => __( 'Copyright', 'nieuw' ),
					'description' => __( 'Copyright text in the site footer.', 'nieuw' ),
					'section'     => 'nieuw',
					'settings'    => 'copyright',
					'type'        => 'text',
				)
			)
		);
	}
}
