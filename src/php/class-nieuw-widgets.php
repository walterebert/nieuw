<?php
/**
 * Widgets
 *
 * @package WordPress
 * @subpackage Nieuw
 */

/**
 * Widgets class
 */
class Nieuw_Widgets {
	/**
	 * Register sidebars
	 */
	public static function sidebars() {
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer', 'nieuw' ),
				'id'            => 'footer-sidebar',
				'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'nieuw' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2>',
				'after_title'   => '</h2>',
			)
		);
	}
}
