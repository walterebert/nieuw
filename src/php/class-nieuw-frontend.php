<?php
/**
 * Frontend
 *
 * @package WordPress
 * @subpackage Nieuw
 */

/**
 * Frontend class
 */
class Nieuw_Frontend {
	/**
	 * Archive title prefix
	 *
	 * @param string $prefix Title prefix.
	 */
	public static function archive_title_prefix( $prefix ) {
		if ( is_category() || is_tag() ) {
			return '<span aria-hidden="true">#</span>';
		}

		return $prefix;
	}

	/**
	 * Add theme name to body classes
	 *
	 * @param array $classes CSS classes.
	 */
	public static function body_class( $classes ) {
		return array_merge( $classes, array( 'nieuw-theme' ) );
	}

	/**
	 * Editor colour palette
	 */
	public static function editor_color_palette() {
		$styles = '';

		$palette = Nieuw_Settings::palette_colors();
		$parent = '';
		if ( 'default' !== $palette['name'] ) {
			$parent .= '.palette-' . $palette['name'] . ' ';
		}
		foreach ( $palette['colors'] as $color ) {
			$styles .= $parent . '.has-' . $color['slug'] . '-background-color {background-color: ' . $color['color'] . ';}' . PHP_EOL;
			$styles .= $parent . '.has-' . $color['slug'] . '-color {color: ' . $color['color'] . ';}' . PHP_EOL;
		}

		return trim( $styles );
	}

	/**
	 * Load stylesheets and scripts
	 */
	public static function enqueue_scripts() {
		$dark_mode      = get_theme_mod( 'dark-mode' );
		$post_thumbnail = has_post_thumbnail();
		$stylesheet_uri = get_stylesheet_directory_uri();
		$version        = wp_get_theme()->get( 'Version' );
		$webfonts       = get_theme_mod( 'webfonts' );

		$base_path      = 'dist';
		if ( WP_DEBUG ) {
			$base_path = 'src';
		}
		$asset_uri = "$stylesheet_uri/$base_path";

		wp_enqueue_script(
			'nieuw',
			"$asset_uri/theme.js",
			array(),
			$version,
			true
		);

		/* Set JS theme variables */
		$js_variables = array(
			'navlink' => array(
				'titleClosed' => esc_html__( 'Open navigation', 'nieuw' ),
				'titleOpened' => esc_html__( 'Close navigation', 'nieuw' ),
			),
			'theme_uri' => $asset_uri,
		);
		wp_add_inline_script(
			'nieuw',
			'var nieuw = ' . json_encode( $js_variables ) . ';',
			'before'
		);

		/* Add comment reply helper JS */
		if ( is_singular() && comments_open() ) {
			wp_enqueue_script( 'comment-reply' );
		}

		/* Add theme styles */
		wp_enqueue_style(
			'nieuw',
			"$asset_uri/css/nieuw.css",
			array(),
			$version
		);

		if ( $webfonts ) {
			wp_enqueue_style(
				'nieuw-webfonts',
				"$asset_uri/css/webfonts.css",
				array( 'nieuw' ),
				$version
			);
		}

		// Right to left styles.
		if ( is_rtl() ) {
			wp_enqueue_style(
				'nieuw-rtl',
				"$asset_uri/css/rtl.css",
				array( 'nieuw' ),
				$version
			);
		}

		wp_enqueue_style(
			'nieuw-icons',
			"$asset_uri/css/icons.css",
			array( 'nieuw' ),
			$version
		);

		/* Add dark mode styles */
		if ( $dark_mode ) {
			wp_enqueue_style(
				'nieuw-dark-mode',
				"$asset_uri/css/dark-mode.css",
				array( 'nieuw' ),
				$version
			);
		}

		/* Editor colour palette */
		wp_add_inline_style( 'nieuw', self::editor_color_palette() );

		/* Add background to navigation icon if the post has a feature image */
		if ( $post_thumbnail ) {
			wp_add_inline_style( 'nieuw', '#navlink {background-color: rgba(255, 255, 255, 0.6);}' );
		}
		if ( $post_thumbnail && $dark_mode ) {
			wp_add_inline_style( 'nieuw-dark-mode', '@media (prefers-color-scheme: dark) {#navlink {background-color: rgba(0, 0, 0, 0.6);}}' );
		}

		/* Add block styles */
		$blocks = Nieuw_Settings::get_blocks();
		$blocks_styles = '';
		foreach ( $blocks as $block_name ) {
			$handle = "wp-block-$block_name";
			$path = "$base_path/css/blocks/$block_name.css";
			$styles = file_get_contents( get_theme_file_path( $path ) );
			if ( wp_should_load_separate_core_block_assets() ) {
				wp_add_inline_style( $handle, $styles );
			} else {
				$blocks_styles .= $styles . PHP_EOL;
			}
		}
		if ( $blocks_styles ) {
			wp_add_inline_style( 'nieuw', $blocks_styles );
		}

		/* Print styles */
		wp_enqueue_style(
			'nieuw-print',
			"$asset_uri/css/print.css",
			array(),
			$version,
			'print'
		);

		/* Load custom styles for Contact Form 7 plugin */
		if ( defined( 'WPCF7_VERSION' ) ) {
			wp_enqueue_style(
				'nieuw-contact-form-7',
				"$asset_uri/css/plugins/contact-form-7.css",
				array( 'nieuw' ),
				$version
			);
		}

		/* Load custom styles for Toolbelt plugin */
		if ( defined( 'TOOLBELT_VERSION' ) ) {
			wp_enqueue_style(
				'nieuw-wp-toolbelt',
				"$asset_uri/css/plugins/wp-toolbelt.css",
				array( 'nieuw' ),
				$version
			);
		}
	}

	/**
	 * Customise img sizes attribute
	 *
	 * @param string $sizes Value of sizes attribute.
	 */
	public static function wp_calculate_image_sizes( $sizes ) {
		if ( is_home() ) {
			return '190px';
		}

		return $sizes;
	}

	/**
	 * Customise img loading attribute
	 *
	 * @param boolean $default  Default value.
	 * @param string  $tag_name Element name.
	 * @param string  $context  Element context.
	 */
	public static function wp_lazy_loading_enabled( $default, $tag_name, $context ) {
		if ( is_single() && 'img' === $tag_name && 'wp_get_attachment_image' === $context ) {
			return false;
		}

		return $default;
	}

	/**
	 * Custom excerpt more link
	 *
	 * @param string $string More text.
	 */
	public static function more( $string ) {
		return ' <a href="' . esc_url( get_permalink() ) . '" class="more" tabindex="-1">' .
			wp_kses(
				sprintf(
					/* translators: %s: Name of current post. After the text, there is a horizontal ellipsis (…). */
					__( 'Continue reading <span class="screen-reader-text">%s</span><span aria-hidden="true">…</span>', 'nieuw' ),
					get_the_title()
				),
				array(
					'span' => array(
						'class' => true,
						'aria-hidden' => true,
					),
				)
			) . '</a> ';
	}

	/**
	 * Set resource hints
	 */
	public static function resource_hints() {
		if ( get_theme_mod( 'webfonts' ) ) {
			$stylesheet_uri = get_stylesheet_directory_uri();

			echo '<link rel="preload" href="' . esc_url( $stylesheet_uri . '/fonts/bitter-wght-latin1.woff2' ) . '" as="font" type="font/woff2" />' . PHP_EOL;
			echo '<link rel="preload" href="' . esc_url( $stylesheet_uri . '/fonts/exo2-wght-latin1.woff2' ) . '" as="font" type="font/woff2" />' . PHP_EOL;
		}
	}
}
